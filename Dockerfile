FROM openjdk:11.0.8 

COPY trade-rest-api/target/trade-rest-api-0.0.1-SNAPSHOT.jar springapp.jar

EXPOSE 8111

# Uncomment to run on docker; Leave commented out to run on localhost
ENV DB_HOST=tradedb:27017
ENV DB_NAME=tradedb

ENTRYPOINT ["java", "-jar", "/springapp.jar"]