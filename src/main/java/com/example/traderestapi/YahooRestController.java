package com.example.traderestapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/yahoo")
@CrossOrigin
public class YahooRestController {

    @Autowired
    public YahooRestClient yahooRestClient;

    @GetMapping(value="/getMovers")
    public ResponseEntity<String> getYahooMovers() {
        String result = yahooRestClient.getMovers();
        log.info("Call to Yahoo RapidAPI get-movers");
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value="/getTrendingTickers")
    public ResponseEntity<String> getYahooTrendingTickers() {
        String result = yahooRestClient.getTrendingTickers();
        log.info("Call to Yahoo RapidAPI get-trending-tickers");
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value="/getQuotes/{symbol}/{region}")
    public ResponseEntity<String> getYahooQuotes(@PathVariable String symbol, @PathVariable String region) {
        String result = yahooRestClient.getQuotes(symbol, region);
        log.info("Call to Yahoo RapidAPI get-quotes");
        return ResponseEntity.ok().body(result);
    }
}
