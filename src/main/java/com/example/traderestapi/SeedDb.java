package com.example.traderestapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.math.BigInteger;
import java.sql.Date;

@Component
public class SeedDb {

    @Autowired
    private TradeService tradeService;

    @PostConstruct
    public void init() {
        tradeService.save(new Trade(BigInteger.ONE, TradeType.BUY,"NYSE",50,62));
    }

    @PreDestroy
    public void cleanup() {
        tradeService.deleteAll();
    }
}
