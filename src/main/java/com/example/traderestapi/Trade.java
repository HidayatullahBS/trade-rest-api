package com.example.traderestapi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@ToString
public class Trade {

    @Transient
    public static final String SEQUENCE_NAME = "trade_sequence";

    @Id
    private BigInteger tradeId;
    private Date created = new Date(System.currentTimeMillis());
    private TradeState state = TradeState.CREATED;
    private TradeType type = TradeType.BUY;
    private String ticker;
    private int quantity;
    private double price;

    public Trade(BigInteger tradeId, TradeType type,String ticker, int quantity, double price)
    {

        this.tradeId = tradeId;
        this.type = type;
        this.ticker =ticker;
        this.quantity =quantity;
        this.price = price;

    }
}
