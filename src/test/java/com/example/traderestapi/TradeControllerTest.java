package com.example.traderestapi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TradeControllerTest {

    @InjectMocks
    TradeRestController tradeRestController;

    @MockBean
    private TradeService tradeService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByState_ReturnTrade() {

        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);
        List<Trade> list = new ArrayList<Trade>();
        list.add(trade);

        //expected
        when(tradeService.findTradesByState(TradeState.CREATED)).thenReturn(list);

        assertEquals(tradeRestController.findByState("CREATED").getStatusCodeValue(), 200);

        verify(tradeService).findTradesByState(TradeState.CREATED);
    }

    @Test
    public void createNewTrade_returnTrade() {
        Trade trade = new Trade(BigInteger.TWO, TradeType.BUY,"NYSE",50,62);
        when(tradeService.createNewTrade(anyObject())).thenReturn(2l);

        ResponseEntity responseEntity =tradeRestController.createNewTrade(trade);

        assertEquals(responseEntity.getStatusCodeValue(), 201);
        assertEquals(responseEntity.getHeaders().getLocation().getPath(), "/2");
        verify(tradeService).createNewTrade(anyObject());

    }
    @Test
    public void findTradeById_returnTrade()
    {
        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);
        when(tradeService.findTradeByTradeId(eq(BigInteger.ONE))).thenReturn(trade);

        assertEquals(tradeRestController.retrieveTradeById(1).getStatusCodeValue(), 200);

        verify(tradeService).findTradeByTradeId(any());

    }

    @Test
    public void retrieveAllTradeRecords_returnAllTrades()
    {
        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);

        //actual
        List<Trade> list = new ArrayList<>();
        list.add(trade);

        //expected
        when(tradeService.retrieveAllTrades()).thenReturn(list);

        assertEquals(tradeRestController.retrieveAllTradeRecords().getStatusCodeValue(), 200);

        verify(tradeService).retrieveAllTrades();

    }
}
