package com.example.traderestapi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TradeServiceTest {

    @MockBean
    private TradeService tradeService;
    private TradeState tradeState;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findTradesByState_ReturnTrade() {

        System.out.println("1");
        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);
        List<Trade> state = tradeService.findTradesByState(tradeState);
        state.add(trade);

        //expected
        when(tradeService.findTradesByState(TradeState.CREATED)).thenReturn(state);
        System.out.println(trade);
        assertEquals(tradeService.findTradesByState(TradeState.CREATED), state);

        verify(tradeService).findTradesByState(TradeState.CREATED);
    }

    @Test
    public void createNewTrade_returnTrade() {
        System.out.println("2");
        Trade trade = new Trade(BigInteger.TWO, TradeType.BUY,"NYSE",50,62);
        when(tradeService.createNewTrade(anyObject())).thenReturn(2l);

        Long id = tradeService.createNewTrade(trade);

        assertSame(id, 2l);
        System.out.println(trade);
        verify(tradeService).createNewTrade(anyObject());
        //assertEquals(tradeService.createNewTrade(trade),trade);


    }
    @Test
    public void findTradeByTradeId_returnTrade()
    {
        System.out.println("3");
        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);
        when(tradeService.findTradeByTradeId(eq(BigInteger.ONE))).thenReturn(trade);

        assertEquals(tradeService.findTradeByTradeId(BigInteger.ONE), trade);
        System.out.println(trade);
        verify(tradeService).findTradeByTradeId(any());

    }
    @Test
    public void retrieveAllTrades_returnAllTrade()
    {
        System.out.println("4");
        Trade trade = new Trade(BigInteger.ONE, TradeType.BUY,"NYSA",20,30);

        //actual
        List<Trade> actual = new ArrayList<>();
        actual.add(trade);

        //expected
        when(tradeService.retrieveAllTrades()).thenReturn(actual);
        System.out.println(trade);
        assertEquals(tradeService.retrieveAllTrades(), actual);

        verify(tradeService).retrieveAllTrades();

    }
}
